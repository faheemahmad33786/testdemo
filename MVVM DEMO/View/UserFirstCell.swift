//
//  UserFirstCell.swift
//  MVVM DEMO
//
//  Created by Himanshu on 7/30/21.
//

import UIKit

class UserFirstCell: UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    var modelUser: Items?{
        didSet{
            userConfiguration()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func userConfiguration(){
        lblTitle.text = "\(modelUser?.title ?? "")"
        lblDate.text = "\(modelUser?.pubDate ?? "")"
        //imgView?.sd_setImage(with: URL(string: "\(modelUser?.enclosure?.link ?? "")"), placeholderImage: UIImage(named: "placeholder"))
        imgView?.sd_setImage(with: URL(string: ""), placeholderImage: UIImage(named: "placeholder"))
    }
    
}
