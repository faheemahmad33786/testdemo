//
//  ViewController.swift
//  MVVM DEMO
//
//  Created by Himanshu on 7/24/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var tblView: UITableView!
    var viewModelUser = UserViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.register(UINib(nibName: "UserCell", bundle: nil), forCellReuseIdentifier: "UserCell")
        tblView.register(UINib(nibName: "UserFirstCell", bundle: nil), forCellReuseIdentifier: "UserFirstCell")
        viewModelUser.vc = self
        viewModelUser.getAllUserData()
    }
    
}

extension ViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModelUser.arrUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserFirstCell", for: indexPath) as? UserFirstCell
            cell?.modelUser = viewModelUser.arrUsers[indexPath.row]
            return cell!
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "UserCell", for: indexPath) as? UserCell
            cell?.modelUser = viewModelUser.arrUsers[indexPath.row]
            return cell!
        }
    }
}



