//
//  UserViewModel.swift
//  MVVM DEMO
//
//

import UIKit

class UserViewModel{
    
    weak var vc: ViewController?
    var arrUsers = [Items]()
    
    
    func getAllUserData(){
        URLSession.shared.dataTask(with: URL(string: BASEURL + getFeed)!) { (data, response, error) in
            if error == nil{
                if let data = data {
                    do{
                        let userResponse = try JSONDecoder().decode(UserModel.self, from: data)
                        self.arrUsers.append(contentsOf: userResponse.items!)
                        DispatchQueue.main.async{
                            self.vc?.tblView.reloadData()
                        }
                    }catch let err{
                        print(err.localizedDescription)
                    }
                }
            }else{
                print(error?.localizedDescription)
            }
        }.resume()
    }
}

